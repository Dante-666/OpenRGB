/*-----------------------------------------*\
|  MSIB450GamingProCarbonMLC.cpp	    |
|                                           |
|  Driver for MSI Mystic Light USB          |
|  lighting controller                      |
|                                           |
|  Dante-666 25/08/2020                     |
\*-----------------------------------------*/

#include "MSIMysticLightController.h"
#include <vector>

struct FeaturePacket_B450 {
    const unsigned char report_id = 0x52; // 0 Report ID
    ZoneData j_rgb_1;                     // 1
    ZoneData j_rainbow_1;                 // 11
    ZoneData j_corsair_fan_ll_1;          // 21
    ZoneData j_corsair_fan_ll_2;          // 31
    ZoneData all_board;                   // -> 41
    // if (!this.b7B12_Special)
    // [45] = 0x29 (zoneflag)
    // [49] = 0x80 (int ColorSel + bool bAllBoard)
    ZoneData on_board_led_1;     // 51
    ZoneData on_board_led_2;     // 61
    ZoneData on_board_led_3;     // 71
    ZoneData on_board_led_4;     // 81
    ZoneData on_board_led_5;     // 91
    ZoneData on_board_led_6;     // 101
    ZoneData j_pipe_1;           // 111
    ZoneData j_pipe_2;           // 121
    ZoneData j_pipe_3;           // 131
    ZoneData j_pipe_4;           // 141
    ZoneData j_rgb_2;            // 151
    unsigned char save_data = 0; // 161
};

class MSIB450GamingProCarbonMLC : public MSIMysticLightController {
  public:
    MSIB450GamingProCarbonMLC(hid_device *handle, const char *path);

    unsigned int GetZoneMinLedCount(unsigned msi_zone) override;

    unsigned int GetZoneMaxLedCount(unsigned msi_zone) override;

    ZoneData *GetZoneData(unsigned msi_zone) override;

    RainbowZoneData *GetRainbowZoneData(unsigned msi_zone) override;

    void SaveOnUpdate(bool send) override;

    void SetDeviceSettings(bool stripe_or_fan, MSI_FAN_TYPE fan_type,
                           unsigned char corsair_device_quantity,
                           bool is_LL120Outer_individual) override;

    void GetDeviceSettings(bool &stripe_or_fan, MSI_FAN_TYPE &fan_type,
                           unsigned char &corsair_device_quantity,
                           bool &is_LL120Outer_individual) override;

    void SetBoardSyncSettings(bool onboard_sync, bool combine_JRGB,
                              bool combine_JPIPE1, bool combine_JPIPE2,
                              bool combine_JRAINBOW1, bool combine_JRAINBOW2,
                              bool combine_crossair) override;

    void GetBoardSyncSettings(bool &onboard_sync, bool &combine_JRGB,
                              bool &combine_JPIPE1, bool &combine_JPIPE2,
                              bool &combine_JRAINBOW1, bool &combine_JRAINBOW2,
                              bool &combine_crossair) override;

    const std::vector<ZoneDescription> &GetLedZoneDescription() const override;

    bool ReadSettings() override;

  private:
    FeaturePacket_B450 data;
    std::vector<ZoneDescription> led_zones;
};
