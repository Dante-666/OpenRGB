#include "MSIMysticLightController.h"

struct FeaturePacket_X570 {
    const unsigned char report_id = 0x52; // Report ID
    ZoneData j_rgb_1;                     // 1
    ZoneData j_pipe_1;                    // 11
    ZoneData j_pipe_2;                    // 21
    RainbowZoneData j_rainbow_1;          // 31
    RainbowZoneData j_rainbow_2;          // 42
    CorsairZoneData j_corsair;            // 53
    ZoneData j_corsair_outerll120;        // 64
    ZoneData on_board_led;                // 74
    ZoneData on_board_led_1;              // 84
    ZoneData on_board_led_2;              // 94
    ZoneData on_board_led_3;              // 104
    ZoneData on_board_led_4;              // 114
    ZoneData on_board_led_5;              // 124
    ZoneData on_board_led_6;              // 134
    ZoneData on_board_led_7;              // 144
    ZoneData on_board_led_8;              // 154
    ZoneData on_board_led_9;              // 164
    ZoneData j_rgb_2;                     // 174
    unsigned char save_data = 0;          // 184
};

class MSIX570GamingPlusMLC : public MSIMysticLightController {
  public:
    MSIX570GamingPlusMLC(hid_device *handle, const char *path);

    unsigned int GetZoneMinLedCount(unsigned msi_zone) override;

    unsigned int GetZoneMaxLedCount(unsigned msi_zone) override;

    ZoneData *GetZoneData(unsigned msi_zone) override;

    RainbowZoneData *GetRainbowZoneData(unsigned msi_zone) override;

    void SaveOnUpdate(bool send) override;

    void SetDeviceSettings(bool stripe_or_fan, MSI_FAN_TYPE fan_type,
                           unsigned char corsair_device_quantity,
                           bool is_LL120Outer_individual) override;

    void GetDeviceSettings(bool &stripe_or_fan, MSI_FAN_TYPE &fan_type,
                           unsigned char &corsair_device_quantity,
                           bool &is_LL120Outer_individual) override;

    void SetBoardSyncSettings(bool onboard_sync, bool combine_JRGB,
                              bool combine_JPIPE1, bool combine_JPIPE2,
                              bool combine_JRAINBOW1, bool combine_JRAINBOW2,
                              bool combine_crossair) override;

    void GetBoardSyncSettings(bool &onboard_sync, bool &combine_JRGB,
                              bool &combine_JPIPE1, bool &combine_JPIPE2,
                              bool &combine_JRAINBOW1, bool &combine_JRAINBOW2,
                              bool &combine_crossair) override;

    const std::vector<ZoneDescription> &GetLedZoneDescription() const override;

    bool ReadSettings() override;

  private:
    FeaturePacket_X570 data;
    std::vector<ZoneDescription> led_zones;
};
