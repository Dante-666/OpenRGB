/*-----------------------------------------*\
|  MSIB450GamingProCarbonMLC.cpp	    |
|                                           |
|  Driver for MSI Mystic Light USB          |
|  lighting controller                      |
|                                           |
|  Dante-666 25/08/2020                     |
\*-----------------------------------------*/

#include "MSIB450GamingProCarbonMLC.h"

namespace {
enum MSI_ZONE_B450 {
    MSI_ZONE_J_RGB_1 = 1,
    MSI_ZONE_J_RAINBOW_1 = 11,
    MSI_ZONE_J_CORSAIR_FAN_LL_1 = 21,
    MSI_ZONE_J_CORSAIR_FAN_LL_2 = 31,
    MSI_ZONE_ALL_BOARD = 41,
    MSI_ZONE_ON_BOARD_LED_1 = 51,
    MSI_ZONE_ON_BOARD_LED_2 = 61,
    MSI_ZONE_ON_BOARD_LED_3 = 71,
    MSI_ZONE_ON_BOARD_LED_4 = 81,
    MSI_ZONE_ON_BOARD_LED_5 = 91,
    MSI_ZONE_ON_BOARD_LED_6 = 101,
    MSI_ZONE_J_PIPE_1 = 111,
    MSI_ZONE_J_PIPE_2 = 121,
    MSI_ZONE_J_PIPE_3 = 131,
    MSI_ZONE_J_PIPE_4 = 141,
    MSI_ZONE_J_RGB_2 = 151
};
}

MSIB450GamingProCarbonMLC::MSIB450GamingProCarbonMLC(hid_device *handle,
                                                     const char *path)
    : MSIMysticLightController(handle, path) {

    led_zones = {ZoneDescription{"JRGB1", MSI_ZONE_J_RGB_1},
                 ZoneDescription{"JRAINBOW1", MSI_ZONE_J_RAINBOW_1},
                 ZoneDescription{"JCORSAIR FAN 1", MSI_ZONE_J_CORSAIR_FAN_LL_1},
                 ZoneDescription{"JCORSAIR FAN 2", MSI_ZONE_J_CORSAIR_FAN_LL_2},
                 ZoneDescription{"Onboard LED 0", MSI_ZONE_ALL_BOARD},
                 ZoneDescription{"Onboard LED 1", MSI_ZONE_ON_BOARD_LED_1},
                 ZoneDescription{"Onboard LED 2", MSI_ZONE_ON_BOARD_LED_2},
                 ZoneDescription{"Onboard LED 3", MSI_ZONE_ON_BOARD_LED_3},
                 ZoneDescription{"Onboard LED 4", MSI_ZONE_ON_BOARD_LED_4},
                 ZoneDescription{"Onboard LED 5", MSI_ZONE_ON_BOARD_LED_5},
                 ZoneDescription{"Onboard LED 6", MSI_ZONE_ON_BOARD_LED_6},
                 ZoneDescription{"JPIPE1", MSI_ZONE_J_PIPE_1},
                 ZoneDescription{"JPIPE2", MSI_ZONE_J_PIPE_2},
                 ZoneDescription{"JPIPE3", MSI_ZONE_J_PIPE_3},
                 ZoneDescription{"JPIPE4", MSI_ZONE_J_PIPE_4},
                 ZoneDescription{"JPIPE4", MSI_ZONE_J_PIPE_4},
                 ZoneDescription{"JRGB2", MSI_ZONE_J_RGB_2}};

    if(handle) {
	ReadSettings();
    }
    /*MSI_MODE mode;
    MSI_SPEED speed;
    MSI_BRIGHTNESS brightness;
    bool rainbow_color;
    this->GetMode(MSI_ZONE_J_RAINBOW_1, mode, speed, brightness, rainbow_color);*/
    std::pair<Color, Color> col1 = this->GetZoneColor(MSI_ZONE_J_RAINBOW_1);
    std::pair<Color, Color> col2 = this->GetZoneColor(MSI_ZONE_ON_BOARD_LED_1);
    std::pair<Color, Color> col3 = this->GetZoneColor(MSI_ZONE_ON_BOARD_LED_2);
    std::pair<Color, Color> col4 = this->GetZoneColor(MSI_ZONE_ON_BOARD_LED_3);
    std::pair<Color, Color> col5 = this->GetZoneColor(MSI_ZONE_ON_BOARD_LED_4);
    std::pair<Color, Color> col6 = this->GetZoneColor(MSI_ZONE_ON_BOARD_LED_5);
    std::pair<Color, Color> col7 = this->GetZoneColor(MSI_ZONE_ON_BOARD_LED_6);

}

unsigned int MSIB450GamingProCarbonMLC::GetZoneMinLedCount(unsigned msi_zone) {
    switch (msi_zone) {
    default:
        return 1;
    }
}

unsigned int MSIB450GamingProCarbonMLC::GetZoneMaxLedCount(unsigned msi_zone) {
    switch (msi_zone) {
    case MSI_ZONE_J_RAINBOW_1:
        return 4; // TODO: It can be different by zone and by mobo
    default:
        return 1;
    }
}

ZoneData *MSIB450GamingProCarbonMLC::GetZoneData(unsigned msi_zone) {
    switch (msi_zone) {
    case MSI_ZONE_J_RGB_1:
        return &data.j_rgb_1;
    case MSI_ZONE_J_RAINBOW_1:
        return &data.j_rainbow_1;
    case MSI_ZONE_J_CORSAIR_FAN_LL_1:
        return &data.j_corsair_fan_ll_1;
    case MSI_ZONE_J_CORSAIR_FAN_LL_2:
        return &data.j_corsair_fan_ll_2;
    case MSI_ZONE_ALL_BOARD:
        return &data.all_board;
    case MSI_ZONE_ON_BOARD_LED_1:
        return &data.on_board_led_1;
    case MSI_ZONE_ON_BOARD_LED_2:
        return &data.on_board_led_2;
    case MSI_ZONE_ON_BOARD_LED_3:
        return &data.on_board_led_3;
    case MSI_ZONE_ON_BOARD_LED_4:
        return &data.on_board_led_4;
    case MSI_ZONE_ON_BOARD_LED_5:
        return &data.on_board_led_5;
    case MSI_ZONE_ON_BOARD_LED_6:
        return &data.on_board_led_6;
    case MSI_ZONE_J_PIPE_1:
        return &data.j_pipe_1;
    case MSI_ZONE_J_PIPE_2:
        return &data.j_pipe_2;
    case MSI_ZONE_J_PIPE_3:
        return &data.j_pipe_3;
    case MSI_ZONE_J_PIPE_4:
        return &data.j_pipe_4;
    case MSI_ZONE_J_RGB_2:
        return &data.j_rgb_2;
    default:
        break;
    }

    return nullptr;
}

RainbowZoneData *
MSIB450GamingProCarbonMLC::GetRainbowZoneData(unsigned msi_zone) {
    switch (msi_zone) {
    default:
        return nullptr;
    }
}

void MSIB450GamingProCarbonMLC::SaveOnUpdate(bool save) {
    // data.save_data = save;
}
void MSIB450GamingProCarbonMLC::SetDeviceSettings(
    bool is_fan, MSI_FAN_TYPE fan_type, unsigned char corsair_device_quantity,
    bool is_LL120Outer_individual) {
    /*-----------------------------------------------------*\
    | If is_fan is false, it is an LED strip                |
    \*-----------------------------------------------------*/
    /*CorsairZoneData &settingsZone = data.j_corsair;
    settingsZone.fan_flags =
        (settingsZone.fan_flags & 0x80) | fan_type << 1 | is_fan;
    settingsZone.corsair_quantity = corsair_device_quantity << 2;
    settingsZone.is_individual =
        BitSet(settingsZone.is_individual, is_LL120Outer_individual, 0);*/
}

void MSIB450GamingProCarbonMLC::GetDeviceSettings(
    bool &stripe_or_fan, MSI_FAN_TYPE &fan_type,
    unsigned char &corsair_device_quantity, bool &is_LL120Outer_individual) {
    /*CorsairZoneData &settingsZone = data.j_corsair;
    stripe_or_fan = settingsZone.fan_flags & 0x01;
    fan_type = (MSI_FAN_TYPE)((settingsZone.fan_flags & 14u) >> 1);
    corsair_device_quantity = (settingsZone.corsair_quantity & 0xFC) >> 2;
    is_LL120Outer_individual = settingsZone.is_individual & 0x01;*/
}

void MSIB450GamingProCarbonMLC::SetBoardSyncSettings(
    bool onboard_sync, bool combine_JRGB, bool combine_JPIPE1,
    bool combine_JPIPE2, bool combine_JRAINBOW1, bool combine_JRAINBOW2,
    bool combine_crossair) {
    // ZoneData &syncZone = data.on_board_led;

    /*-----------------------------------------------------*\
    | Set sync flags for on-board LED zone                  |
    \*-----------------------------------------------------*/
    /*syncZone.colorFlags = BitSet(syncZone.colorFlags, onboard_sync, 0);
    syncZone.colorFlags = BitSet(syncZone.colorFlags, combine_JRAINBOW1, 1);
    syncZone.colorFlags = BitSet(syncZone.colorFlags, combine_JRAINBOW2, 2);
    syncZone.colorFlags = BitSet(syncZone.colorFlags, combine_crossair, 3);
    syncZone.colorFlags = BitSet(syncZone.colorFlags, combine_JPIPE1, 4);
    syncZone.colorFlags = BitSet(syncZone.colorFlags, combine_JPIPE2, 5);

    syncZone.speedAndBrightnessFlags =
        BitSet(syncZone.speedAndBrightnessFlags, combine_JRGB, 7);*/
}

void MSIB450GamingProCarbonMLC::GetBoardSyncSettings(
    bool &onboard_sync, bool &combine_JRGB, bool &combine_JPIPE1,
    bool &combine_JPIPE2, bool &combine_JRAINBOW1, bool &combine_JRAINBOW2,
    bool &combine_crossair) {
    // ZoneData &syncZone = data.on_board_led;

    /*-----------------------------------------------------*\
    | Get sync flags for on-board LED zone                  |
    \*-----------------------------------------------------*/
    /*onboard_sync = (syncZone.colorFlags >> 0) & 0x01;
    combine_JRAINBOW1 = (syncZone.colorFlags >> 1) & 0x01;
    combine_JRAINBOW2 = (syncZone.colorFlags >> 2) & 0x01;
    combine_crossair = (syncZone.colorFlags >> 3) & 0x01;
    combine_JPIPE1 = (syncZone.colorFlags >> 4) & 0x01;
    combine_JPIPE2 = (syncZone.colorFlags >> 5) & 0x01;

    combine_JRGB = (syncZone.speedAndBrightnessFlags & 0x80) >> 7;*/
}

const std::vector<ZoneDescription> &
MSIB450GamingProCarbonMLC::GetLedZoneDescription() const {
    return led_zones;
}

bool MSIB450GamingProCarbonMLC::ReadSettings() {
    /*-----------------------------------------------------*\
    | Read packet from hardware, return true if successful  |
    \*-----------------------------------------------------*/
    return (hid_get_feature_report(dev, (unsigned char *)&data, sizeof(data)) ==
            sizeof data);
}
