/*-----------------------------------------*\
|  MSIMysticLightController.cpp             |
|                                           |
|  Driver for MSI Mystic Light USB          |
|  lighting controller                      |
|                                           |
|  T-bond 3/4/2020                          |
\*-----------------------------------------*/

#include "MSIMysticLightController.h"
#include <algorithm>
#include <array>
#include <bitset>

MSIMysticLightController::MSIMysticLightController(hid_device *handle,
                                                   const char *path) {
    dev = handle;
    if (dev) {
        loc = path;

        ReadName();
        ReadSerial();
        ReadFwVersion();
    }
}

MSIMysticLightController::~MSIMysticLightController() {
    if (dev) {
        hid_close(dev);
    }
}

unsigned int MSIMysticLightController::GetZoneLedCount(unsigned msi_zone) {
    RainbowZoneData *requestedZone = GetRainbowZoneData(msi_zone);

    if (!requestedZone) {
        return GetZoneMaxLedCount(msi_zone);
    }

    return requestedZone->cycle_or_led_num;
}

void MSIMysticLightController::SetZoneLedCount(unsigned msi_zone,
                                               unsigned int led_count) {
    RainbowZoneData *requestedZone = GetRainbowZoneData(msi_zone);

    if (!requestedZone) {
        return;
    }

    led_count = std::min(GetZoneMaxLedCount(msi_zone),
                         std::max(GetZoneMinLedCount(msi_zone), led_count));
    requestedZone->cycle_or_led_num = led_count;
}

void MSIMysticLightController::SetMode(unsigned msi_zone, MSI_MODE mode, MSI_SPEED speed,
                                       MSI_BRIGHTNESS brightness,
                                       bool rainbow_color) {
    ZoneData *zoneData = GetZoneData(msi_zone);
    if (!zoneData) {
        return;
    }

    zoneData->effect = mode;
    zoneData->speedAndBrightnessFlags =
        (zoneData->speedAndBrightnessFlags & 128u) | brightness << 2u | speed;
    zoneData->colorFlags = BitSet(zoneData->colorFlags, !rainbow_color, 7u);
}

void MSIMysticLightController::GetMode(unsigned msi_zone, MSI_MODE &mode,
                                       MSI_SPEED &speed,
                                       MSI_BRIGHTNESS &brightness,
                                       bool &rainbow_color) {
    /*-----------------------------------------------------*\
    | Get data for given zone                               |
    \*-----------------------------------------------------*/
    ZoneData *zoneData = GetZoneData(msi_zone);

    /*-----------------------------------------------------*\
    | Return if zone is invalid                             |
    \*-----------------------------------------------------*/
    if (!zoneData) {
        return;
    }

    /*-----------------------------------------------------*\
    | Update pointers with data                             |
    \*-----------------------------------------------------*/
    mode = (MSI_MODE)(zoneData->effect);
    speed = (MSI_SPEED)(zoneData->speedAndBrightnessFlags & 0x03);
    brightness =
        (MSI_BRIGHTNESS)((zoneData->speedAndBrightnessFlags >> 2) & 0x1F);
    rainbow_color = (zoneData->colorFlags & 0x80) >> 7;
}

void MSIMysticLightController::SetZoneColor(
    unsigned msi_zone, unsigned char red1, unsigned char grn1, unsigned char blu1,
    unsigned char red2, unsigned char grn2, unsigned char blu2) {
    ZoneData *zoneData = GetZoneData(msi_zone);

    if (!zoneData) {
        return;
    }

    zoneData->color.R = red1;
    zoneData->color.G = grn1;
    zoneData->color.B = blu1;

    zoneData->color2.R = red2;
    zoneData->color2.G = grn2;
    zoneData->color2.B = blu2;
}

std::pair<Color, Color> MSIMysticLightController::GetZoneColor(unsigned msi_zone) {
    ZoneData *zoneData = GetZoneData(msi_zone);

    if (!zoneData) {
        return std::make_pair(Color{}, Color{});
    }

    return std::make_pair(
        Color{zoneData->color.R, zoneData->color.G, zoneData->color.B},
        Color{zoneData->color2.R, zoneData->color2.G, zoneData->color2.B});
}

void MSIMysticLightController::SetCycleCount(unsigned msi_zone,
                                             unsigned char cycle_num) {
    RainbowZoneData *requestedZone = GetRainbowZoneData(msi_zone);

    if (!requestedZone) {
        return;
    }

    requestedZone->cycle_or_led_num = cycle_num;
}

unsigned char MSIMysticLightController::GetCycleCount(unsigned msi_zone) {
    RainbowZoneData *requestedZone = GetRainbowZoneData(msi_zone);

    if (!requestedZone) {
        return 0;
    }

    return requestedZone->cycle_or_led_num;
}

bool MSIMysticLightController::SetVolume(unsigned char main, unsigned char left,
                                         unsigned char right) {
    unsigned char packet[64];
    std::fill_n(packet, sizeof packet, 204u);

    if (main > 100u) {
        main = 100u;
    }

    if (left > 100u) {
        left = 100u;
    }

    if (right > 100u) {
        right = 100u;
    }

    packet[0] = 1u;
    packet[1] = 192u;
    packet[3] = main;
    packet[4] = left;
    packet[5] = right;

    // return hid_write(dev, packet, sizeof packet);
    return true;
}

std::string MSIMysticLightController::GetDeviceName() { return name; }

std::string MSIMysticLightController::GetDeviceLocation() { return loc; }

std::string MSIMysticLightController::GetFWVersion() {
    return std::string("AP/LD ")
        .append(version_APROM)
        .append(" / ")
        .append(version_LDROM);
}


std::string MSIMysticLightController::GetSerial() { return chip_id; }

bool MSIMysticLightController::Update() {
    /*-----------------------------------------------------*\
    | Send packet to hardware, return true if successful    |
    \*-----------------------------------------------------*/
    // Disable this for brick issues, try to see what mode is set currently
    return true;
    // return(hid_send_feature_report(dev, (unsigned char *)&data, sizeof(data))
    // == sizeof data);
}

bool MSIMysticLightController::ReadFwVersion() {
    unsigned char request[64];
    unsigned char response[64];
    int ret_val = 64;

    /*-----------------------------------------------------*\
    | First read the APROM                                  |
    | Checksum also available at report ID 180, with MSB    |
    | stored at index 0x08 and LSB at 0x09                  |
    \*-----------------------------------------------------*/

    /*-----------------------------------------------------*\
    | Zero out buffers                                      |
    \*-----------------------------------------------------*/
    memset(request, 0x00, sizeof(request));
    memset(response, 0x00, sizeof(response));

    /*-----------------------------------------------------*\
    | Set up APROM Firmware Version Request packet          |
    \*-----------------------------------------------------*/
    request[0x00] = 0x01;
    request[0x01] = 0xB0;

    /*-----------------------------------------------------*\
    | Fill request from 0x02 to 0x61 with 0xCC              |
    \*-----------------------------------------------------*/
    memset(&request[0x02], 0xCC, sizeof(request) - 2);

    /*-----------------------------------------------------*\
    | Send request and receive response packets             |
    \*-----------------------------------------------------*/
    //ret_val &= hid_write(dev, request, 64);
    //ret_val &= hid_read(dev, response, 64);

    /*-----------------------------------------------------*\
    | Extract high and low values from response             |
    \*-----------------------------------------------------*/
    unsigned char highValue = response[2] >> 4;
    unsigned char lowValue = response[2] & 0x0F;

    /*-----------------------------------------------------*\
    | Build firmware string <high>.<low>                    |
    \*-----------------------------------------------------*/
    version_APROM = std::to_string(static_cast<int>(highValue))
                        .append(".")
                        .append(std::to_string(static_cast<int>(lowValue)));

    /*-----------------------------------------------------*\
    | First read the LDROM                                  |
    | Checksum also available at report ID 184, with MSB    |
    | stored at index 0x08 and LSB at 0x09                  |
    \*-----------------------------------------------------*/

    /*-----------------------------------------------------*\
    | Set up LDROM Firmware Version Request packet          |
    \*-----------------------------------------------------*/
    request[0x00] = 0x01;
    request[0x01] = 0xB6;

    /*-----------------------------------------------------*\
    | Send request and receive response packets             |
    \*-----------------------------------------------------*/
    //ret_val &= hid_write(dev, request, 64);
    //ret_val &= hid_read(dev, response, 64);

    /*-----------------------------------------------------*\
    | Extract high and low values from response             |
    \*-----------------------------------------------------*/
    highValue = response[2] >> 4;
    lowValue = response[2] & 0x0F;

    /*-----------------------------------------------------*\
    | Build firmware string <high>.<low>                    |
    \*-----------------------------------------------------*/
    version_LDROM = std::to_string(static_cast<int>(highValue))
                        .append(".")
                        .append(std::to_string(static_cast<int>(lowValue)));

    /*-----------------------------------------------------*\
    | If return value is zero it means an HID transfer      |
    | failed                                                |
    \*-----------------------------------------------------*/
    return true;
    //return (ret_val > 0);
}

void MSIMysticLightController::ReadSerial() {
    wchar_t serial[256];

    /*-----------------------------------------------------*\
    | Get the serial number string from HID                 |
    \*-----------------------------------------------------*/
    hid_get_serial_number_string(dev, serial, 256);

    /*-----------------------------------------------------*\
    | Convert wchar_t into std::wstring into std::string    |
    \*-----------------------------------------------------*/
    std::wstring wserial = std::wstring(serial);
    chip_id = std::string(wserial.begin(), wserial.end());
}

void MSIMysticLightController::ReadName() {
    wchar_t tname[256];

    /*-----------------------------------------------------*\
    | Get the manufacturer string from HID                  |
    \*-----------------------------------------------------*/
    hid_get_manufacturer_string(dev, tname, 256);

    /*-----------------------------------------------------*\
    | Convert wchar_t into std::wstring into std::string    |
    \*-----------------------------------------------------*/
    std::wstring wname = std::wstring(tname);
    name = std::string(wname.begin(), wname.end());

    /*-----------------------------------------------------*\
    | Get the product string from HID                       |
    \*-----------------------------------------------------*/
    hid_get_product_string(dev, tname, 256);

    /*-----------------------------------------------------*\
    | Append the product string to the manufacturer string  |
    \*-----------------------------------------------------*/
    wname = std::wstring(tname);
    name.append(" ").append(std::string(wname.begin(), wname.end()));
}

unsigned char MSIMysticLightController::BitSet(unsigned char value, bool bit,
                                               unsigned int position) {
    return static_cast<unsigned char>(
        std::bitset<8>(value).set(position, bit).to_ulong());
}
