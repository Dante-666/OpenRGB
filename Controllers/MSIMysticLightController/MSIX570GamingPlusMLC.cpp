/*-----------------------------------------*\
|  MSIX570GamingPlusMLC.cpp	            |
|                                           |
|  Driver for MSI Mystic Light USB          |
|  lighting controller                      |
|                                           |
|  T-bond 3/4/2020                          |
\*-----------------------------------------*/

#include "MSIX570GamingPlusMLC.h"
#include <vector>

namespace {
enum MSI_ZONE_X570 {
    MSI_ZONE_J_RGB_1 = 1,
    MSI_ZONE_J_PIPE_1 = 11,
    MSI_ZONE_J_PIPE_2 = 21,
    MSI_ZONE_J_RAINBOW_1 = 31,
    MSI_ZONE_J_RAINBOW_2 = 42,
    MSI_ZONE_J_CORSAIR = 53,
    MSI_ZONE_J_CORSAIR_OUTERLL120 = 64,
    MSI_ZONE_ON_BOARD_LED = 74,
    MSI_ZONE_ON_BOARD_LED_1 = 84,
    MSI_ZONE_ON_BOARD_LED_2 = 94,
    MSI_ZONE_ON_BOARD_LED_3 = 104,
    MSI_ZONE_ON_BOARD_LED_4 = 114,
    MSI_ZONE_ON_BOARD_LED_5 = 124,
    MSI_ZONE_ON_BOARD_LED_6 = 134,
    MSI_ZONE_ON_BOARD_LED_7 = 144,
    MSI_ZONE_ON_BOARD_LED_8 = 154,
    MSI_ZONE_ON_BOARD_LED_9 = 164,
    MSI_ZONE_J_RGB_2 = 174
};
}

MSIX570GamingPlusMLC::MSIX570GamingPlusMLC(hid_device *handle, const char *path)
    : MSIMysticLightController(handle, path) {

    led_zones = {
        ZoneDescription{"JRGB1", MSI_ZONE_J_RGB_1},
        ZoneDescription{"JRGB2", MSI_ZONE_J_RGB_2},
        ZoneDescription{"JRAINBOW1", MSI_ZONE_J_RAINBOW_1},
        ZoneDescription{"JRAINBOW2", MSI_ZONE_J_RAINBOW_2},
        ZoneDescription{"JPIPE1", MSI_ZONE_J_PIPE_1},
        ZoneDescription{"JPIPE2", MSI_ZONE_J_PIPE_2},
        ZoneDescription{"JCORSAIR", MSI_ZONE_J_CORSAIR},
        ZoneDescription{"JCORSAIR Outer", MSI_ZONE_J_CORSAIR_OUTERLL120},
        ZoneDescription{"Onboard LED 0", MSI_ZONE_ON_BOARD_LED},
        ZoneDescription{"Onboard LED 1", MSI_ZONE_ON_BOARD_LED_1},
        ZoneDescription{"Onboard LED 2", MSI_ZONE_ON_BOARD_LED_2},
        ZoneDescription{"Onboard LED 3", MSI_ZONE_ON_BOARD_LED_3},
        ZoneDescription{"Onboard LED 4", MSI_ZONE_ON_BOARD_LED_4},
        ZoneDescription{"Onboard LED 5", MSI_ZONE_ON_BOARD_LED_5},
        ZoneDescription{"Onboard LED 6", MSI_ZONE_ON_BOARD_LED_6},
        ZoneDescription{"Onboard LED 7", MSI_ZONE_ON_BOARD_LED_7},
        ZoneDescription{"Onboard LED 8", MSI_ZONE_ON_BOARD_LED_8},
        ZoneDescription{"Onboard LED 9", MSI_ZONE_ON_BOARD_LED_9},
    };

    if (handle) {
        ReadSettings();
    }
}

unsigned int MSIX570GamingPlusMLC::GetZoneMinLedCount(unsigned msi_zone) {
    switch (msi_zone) {
    default:
        return 1;
    }
}

unsigned int MSIX570GamingPlusMLC::GetZoneMaxLedCount(unsigned msi_zone) {
    switch (msi_zone) {
    case MSI_ZONE_J_RAINBOW_1:
    case MSI_ZONE_J_RAINBOW_2:
    case MSI_ZONE_J_CORSAIR:
        return 4; // TODO: It can be different by zone and by mobo
    default:
        return 1;
    }
}

ZoneData *MSIX570GamingPlusMLC::GetZoneData(unsigned msi_zone) {
    switch (msi_zone) {
    case MSI_ZONE_J_RGB_1:
        return &data.j_rgb_1;
    case MSI_ZONE_J_RGB_2:
        return &data.j_rgb_2;
    case MSI_ZONE_J_RAINBOW_1:
        return &data.j_rainbow_1;
    case MSI_ZONE_J_RAINBOW_2:
        return &data.j_rainbow_2;
    case MSI_ZONE_J_PIPE_1:
        return &data.j_pipe_1;
    case MSI_ZONE_J_PIPE_2:
        return &data.j_pipe_2;
    case MSI_ZONE_ON_BOARD_LED:
        return &data.on_board_led;
    case MSI_ZONE_ON_BOARD_LED_1:
        return &data.on_board_led_1;
    case MSI_ZONE_ON_BOARD_LED_2:
        return &data.on_board_led_2;
    case MSI_ZONE_ON_BOARD_LED_3:
        return &data.on_board_led_3;
    case MSI_ZONE_ON_BOARD_LED_4:
        return &data.on_board_led_4;
    case MSI_ZONE_ON_BOARD_LED_5:
        return &data.on_board_led_5;
    case MSI_ZONE_ON_BOARD_LED_6:
        return &data.on_board_led_6;
    case MSI_ZONE_ON_BOARD_LED_7:
        return &data.on_board_led_7;
    case MSI_ZONE_ON_BOARD_LED_8:
        return &data.on_board_led_8;
    case MSI_ZONE_ON_BOARD_LED_9:
        return &data.on_board_led_9;
    case MSI_ZONE_J_CORSAIR_OUTERLL120:
        return &data.j_corsair_outerll120;
    default:
    case MSI_ZONE_J_CORSAIR:
        break;
    }

    return nullptr;
}

RainbowZoneData *MSIX570GamingPlusMLC::GetRainbowZoneData(unsigned msi_zone) {
    switch (msi_zone) {
    case MSI_ZONE_J_RAINBOW_1:
        return &data.j_rainbow_1;
    case MSI_ZONE_J_RAINBOW_2:
        return &data.j_rainbow_2;
    case MSI_ZONE_J_CORSAIR:
    default:
        return nullptr;
    }
}

void MSIX570GamingPlusMLC::SaveOnUpdate(bool save) {
    // data.save_data = save;
}
void MSIX570GamingPlusMLC::SetDeviceSettings(
    bool is_fan, MSI_FAN_TYPE fan_type, unsigned char corsair_device_quantity,
    bool is_LL120Outer_individual) {
    /*-----------------------------------------------------*\
    | If is_fan is false, it is an LED strip                |
    \*-----------------------------------------------------*/
    CorsairZoneData &settingsZone = data.j_corsair;
    settingsZone.fan_flags =
        (settingsZone.fan_flags & 0x80) | fan_type << 1 | is_fan;
    settingsZone.corsair_quantity = corsair_device_quantity << 2;
    settingsZone.is_individual =
        BitSet(settingsZone.is_individual, is_LL120Outer_individual, 0);
}

void MSIX570GamingPlusMLC::GetDeviceSettings(
    bool &stripe_or_fan, MSI_FAN_TYPE &fan_type,
    unsigned char &corsair_device_quantity, bool &is_LL120Outer_individual) {
    CorsairZoneData &settingsZone = data.j_corsair;
    stripe_or_fan = settingsZone.fan_flags & 0x01;
    fan_type = (MSI_FAN_TYPE)((settingsZone.fan_flags & 14u) >> 1);
    corsair_device_quantity = (settingsZone.corsair_quantity & 0xFC) >> 2;
    is_LL120Outer_individual = settingsZone.is_individual & 0x01;
}

void MSIX570GamingPlusMLC::SetBoardSyncSettings(
    bool onboard_sync, bool combine_JRGB, bool combine_JPIPE1,
    bool combine_JPIPE2, bool combine_JRAINBOW1, bool combine_JRAINBOW2,
    bool combine_crossair) {
    ZoneData &syncZone = data.on_board_led;

    /*-----------------------------------------------------*\
    | Set sync flags for on-board LED zone                  |
    \*-----------------------------------------------------*/
    syncZone.colorFlags = BitSet(syncZone.colorFlags, onboard_sync, 0);
    syncZone.colorFlags = BitSet(syncZone.colorFlags, combine_JRAINBOW1, 1);
    syncZone.colorFlags = BitSet(syncZone.colorFlags, combine_JRAINBOW2, 2);
    syncZone.colorFlags = BitSet(syncZone.colorFlags, combine_crossair, 3);
    syncZone.colorFlags = BitSet(syncZone.colorFlags, combine_JPIPE1, 4);
    syncZone.colorFlags = BitSet(syncZone.colorFlags, combine_JPIPE2, 5);

    syncZone.speedAndBrightnessFlags =
        BitSet(syncZone.speedAndBrightnessFlags, combine_JRGB, 7);
}

void MSIX570GamingPlusMLC::GetBoardSyncSettings(
    bool &onboard_sync, bool &combine_JRGB, bool &combine_JPIPE1,
    bool &combine_JPIPE2, bool &combine_JRAINBOW1, bool &combine_JRAINBOW2,
    bool &combine_crossair) {
    ZoneData &syncZone = data.on_board_led;

    /*-----------------------------------------------------*\
    | Get sync flags for on-board LED zone                  |
    \*-----------------------------------------------------*/
    onboard_sync = (syncZone.colorFlags >> 0) & 0x01;
    combine_JRAINBOW1 = (syncZone.colorFlags >> 1) & 0x01;
    combine_JRAINBOW2 = (syncZone.colorFlags >> 2) & 0x01;
    combine_crossair = (syncZone.colorFlags >> 3) & 0x01;
    combine_JPIPE1 = (syncZone.colorFlags >> 4) & 0x01;
    combine_JPIPE2 = (syncZone.colorFlags >> 5) & 0x01;

    combine_JRGB = (syncZone.speedAndBrightnessFlags & 0x80) >> 7;
}

const std::vector<ZoneDescription> &
MSIX570GamingPlusMLC::GetLedZoneDescription() const {
    return led_zones;
}

bool MSIX570GamingPlusMLC::ReadSettings() {
    /*-----------------------------------------------------*\
    | Read packet from hardware, return true if successful  |
    \*-----------------------------------------------------*/
    return (hid_get_feature_report(dev, (unsigned char *)&data, sizeof(data)) ==
            sizeof data);
}
